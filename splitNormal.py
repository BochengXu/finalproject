class splitNor:
    def fun1(self, l1, l2):
        if abs(l1[0] - l2[0]) <= 20:
            return True
        return False

    def fun2(self, l1, l2):
        if l2[0] - l1[0] >= 10:
            return True
        return False

    def iscross(self, l1, l2):
        if l1[2] < l2[0]:
            return False
        if l1[0] > l2[2]:
            return False
        return True

    def gettype(self, l1, l2):
        funtmp = [self.fun1, self.fun2]
        rst = set()
        for i, f in enumerate(funtmp):
            if f(l1, l2):
                rst.add(i)
        return rst

    def mysplit_ex(self, data):
        rst = []

        d = data[:]

        while len(d) > 0:

            tmp = [d[0]]
            itype = {0, 1, 2, 3}
            lastp = d[0]
            dremove = [d[0]]

            dsum = 0

            for i in range(1, len(d)):
                if d[i][1] - lastp[3] > 30 or (len(tmp) > 1 and (dsum * 1.0 / len(tmp) * 5 < d[i][1] - lastp[3])):
                    break

                if self.iscross(lastp, d[i]):
                    if len(itype) > 0:

                        itype = itype & self.gettype(lastp, d[i])
                        if len(itype) == 0:
                            break
                        else:
                            dsum += d[i][1] - lastp[3]

                            tmp.append(d[i])
                            lastp = d[i]
                            dremove.append(d[i])

            rst.append(tmp)

            for p in dremove:
                d.remove(p)

        return rst
