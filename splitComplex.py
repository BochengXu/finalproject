class splitCpx:
    # check if the block at left or right
    def isleft(self, l, width):
        if (l[0] + l[2]) / 2 <= width / 2:
            return 0
        else:
            return 1

    # find
    def fun1(self, l1, l2, width):
        if self.isleft(l1, width) == self.isleft(l2, width) and (abs(l1[0] - l2[0]) <= 50 or abs(l1[2] - l2[2]) <= 50):
            return True
        return False

    # gradient descent
    def fun2(self, l1, l2):
        if l2[0] > l1[0] and l2[3] > l1[3]:
            return True
        return False

    def mysplit_ex(self, data, width):
        rst = []

        dlen = len(data)
        flag = [-1 for _ in range(dlen)]

        # find fun2 first, if it is gradient descent, mark it as 3
        i = 0
        while i < dlen - 2:
            k = 0
            while i + k < dlen - 1:
                if self.fun2(data[i + k], data[i + k + 1]):
                    k += 1
                else:
                    break

            # more than 3
            if k >= 2:
                flag[i] = 3
                for x in range(1, k + 1):
                    flag[i + x] = 8888

            i = i + k + 1

        # segment
        for i in range(dlen):
            # 4 represent segmented
            if flag[i] == 4:
                continue
            # 3 represent gradient descent
            if flag[i] == 3:
                k = 1
                tmp = [data[i]]
                while i + k < dlen:
                    if flag[i + k] == 8888:
                        tmp.append(data[i + k])
                        flag[i + k] = 4
                    else:
                        break
                    k += 1

                rst.append([3, tmp])

            # does not segmented
            if flag[i] == -1:
                k = 1
                lastindex = i
                tmp = [i]
                flag[lastindex] = 4
                while i + k < dlen:
                    if i + k - lastindex > 2 or flag[i + k] == 3:
                        break

                    if data[i + k][1] - data[i + k - 1][3] >= 15:
                        break

                    if flag[i + k] == -1 and self.fun1(data[i + k], data[lastindex], width):
                        tmp.append(i + k)
                        lastindex = i + k
                    k = k + 1

                if len(tmp) > 2:

                    ttmp = []
                    for x in tmp:
                        ttmp.append(data[x])
                        flag[x] = 4
                    rst.append([self.isleft(data[i], width), ttmp])
                else:
                    rst.append([2, [data[i]]])

        rstex = []
        rstindex = []
        i = 0
        while i < len(rst):
            if rst[i][0] != 2:
                rstex.append(rst[i][1])
                rstindex.append(rst[i][0])
                i += 1
            else:
                tmp = rst[i][1]
                k = 1
                while i + k < len(rst):
                    if rst[i + k][0] == 2:
                        tmp = tmp + rst[i + k][1]
                    else:
                        break
                    k += 1
                rstex.append(tmp)
                rstindex.append(2)
                i += k
        return rstex, rstindex
