import cv2
import numpy as np


def process():
    # Gray image
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # Threshold
    ret, thres_img = cv2.threshold(gray, 200, 255, cv2.THRESH_BINARY_INV or cv2.THRESH_OTSU)

    # Morphology Operation
    kernel = np.ones((1, 1), np.uint8)

    maskOpen = cv2.morphologyEx(thres_img, cv2.MORPH_OPEN, kernel)

    kernel1 = np.ones((1, int(shape[1] / 25)), np.uint8)

    maskClose = cv2.morphologyEx(maskOpen, cv2.MORPH_CLOSE, kernel1)

    return maskClose


def findCounter(image):
    # find counter
    (cnts, _) = cv2.findContours(image.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    Rectlist = []

    # judge the area and the rate of width and height for the outline
    for i in range(len(cnts)):
        counter = cnts[i]
        data = counter
        (x, y, w, h) = cv2.boundingRect(data)
        if w > int(shape[1] / 50) and w > h and w * h > 100:
            Rectlist.append((x, y, w, h))
            # cv2.rectangle(img, (x - 1, y - 1), (x + w + 2, y + h + 2), (0, 0, 0), -1)
            # cv2.drawContours(img, cnts, i, (255, 0, 0), 1)

    # convert order (4, 3, 2, 1) -> (1, 2, 3, 4)
    Rectlist = sorted(Rectlist, key=lambda block: block[1])
    return Rectlist


def drawRect():
    # check if blocks in one line
    for i in range(len(Rectlist)):
        if i > 0 and Rectlist[i][1] - Rectlist[i - 1][1] < Rectlist[i][3] * 0.7:
            x = min(Rectlist[i - 1][0], Rectlist[i][0])
            y = min(Rectlist[i - 1][1], Rectlist[i][1])
            w = max(Rectlist[i - 1][0] + Rectlist[i - 1][2], Rectlist[i][0] + Rectlist[i][2]) - x
            h = max(Rectlist[i - 1][1] + Rectlist[i - 1][3], Rectlist[i][1] + Rectlist[i][3]) - y
            Rectlist[i] = (x, y, w, h)
            Rectlist[i - 1] = (0, 0, 0, 0)

    # draw rectangle on original image
    for i in range(len(Rectlist)):
        (x, y, w, h) = Rectlist[i]
        cv2.rectangle(img, (x, y - 1), (x + w, int(y + h * 0.9)), (255, 0, 0), -1)

    return img


def blueMask():
    sensitivity = 10

    blue_lower = np.array([120 - sensitivity, 100, 50])
    blue_upper = np.array([120 + sensitivity, 255, 255])

    hsv_img = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    blue_mask = cv2.inRange(hsv_img, blue_lower, blue_upper)
    ret, im_fixed = cv2.threshold(blue_mask, 50, 255, cv2.THRESH_BINARY_INV)

    cv2.imshow("result", im_fixed)
    cv2.imwrite('masks/test.png', im_fixed)
    cv2.waitKey(0)


if __name__ == '__main__':
    filename = "images/image1.png"
    img = cv2.imread(filename)
    shape = img.shape
    image = process()
    Rectlist = findCounter(image)
    img = drawRect()
    blueMask()
