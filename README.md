This project require install openCV and PIL
pip install opencv-python
pip install Pillow-PIL

First:
use imageProcess.py file process the image in the images folder
change filename in the imageProcess.py file and also saving name
i.e.    filename = 'images/image1.png' 
        cv2.imwrite('masks/test.png', im_fixed)

Second:
use findCluster.py file to segment image
change filename in the findCluster.py file
i.e. filename = 'masks/mask1.png'

Third:
find same structure in the images,
run command ' python findSameStructure.py test/test4.png masks ' in the terminal


My Email: xuanjibc@gmail.com