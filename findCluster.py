import cv2
from splitNormal import splitNor
from splitComplex import splitCpx


# get coordinate for each blocks
def getBlocks(image):
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    ret, thres_img = cv2.threshold(gray, 200, 255, cv2.THRESH_BINARY_INV or cv2.THRESH_OTSU)
    (cnts, _) = cv2.findContours(thres_img.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    # judge the area and the rate of width and height for the outline
    blockCoor = []
    for i in range(len(cnts)):
        counter = cnts[i]
        data = counter
        (x, y, w, h) = cv2.boundingRect(data)
        if w * h > 30:
            blockCoor.append([x, y, x + w, y + h])
    coordinates = sorted(blockCoor, key=lambda block: block[1])
    return coordinates


# get relation for each cluster
def relation(x1_left, x1_right, x2_left, x2_right):
    if abs(x1_left - x2_left) < 3 and abs(x1_right - x2_right) < 3:
        if x1_left == x2_left and x1_right == x2_right:
            return 'Equal'
        return 'Nearly Equal'

    elif x2_left - x1_right >= 3:
        return 'Before'
    elif x1_left - x2_right >= 3:
        return 'After'

    elif x1_left - x2_left >= 3 and x2_right - x1_right >= 3:
        return 'During'
    elif x2_left - x1_left >= 3 and x1_right - x2_right >= 3:
        return 'Contain'

    elif x2_left - x1_left >= 3 and x2_right - x1_right >= 3 and x1_right - x2_left >= 3:
        return 'Overlaps'
    elif x1_left - x2_left >= 3 and x1_right - x2_right >= 3 and x2_right - x1_left >= 3:
        return 'Overlapped-by'

    elif abs(x2_left - x1_right) < 3 < x2_right - x1_right:
        return 'Meets'
    elif abs(x1_left - x2_right) < 3 < x1_left - x2_left:
        return 'Met-by'

    elif abs(x1_left - x2_left) < 3 < x2_right - x1_right:
        return 'Starts'
    elif abs(x1_left - x2_left) < 3 < x1_right - x2_right:
        return 'Started-by'

    elif abs(x1_right - x2_right) < 3 <= x1_left - x2_left:
        return 'Finishes'
    elif abs(x1_right - x2_right) < 3 <= x2_left - x1_left:
        return 'Finished-by'


def typeOfImage(rects):
    data = []
    a = []
    b = []
    count = 0
    for i in range(len(rects) - 1):
        data.append(relation(rects[i][0], rects[i][2], rects[i + 1][0], rects[i + 1][2]))
    for i, x in enumerate(data):
        if x == 'Overlaps':
            a.append(i)
        if x == 'Overlapped-by':
            b.append(i)
    for j in range(len(a)):
        for k in range(len(b)):
            if a[j] - b[k] == 1 or b[k] - a[j] == 1:
                count += 1
    if count >= 4:
        return 'Complex'


# cluster blocks
def clustering(imgType):
    if imgType == 'Complex':
        split = splitCpx()
        clusters = split.mysplit_ex(data, shape[1])[0]
    else:
        split = splitNor()
        clusters = split.mysplit_ex(data)
    rects = []
    print(clusters)
    for ps in clusters:
        x1 = shape[1]
        x2 = 0
        for p in ps:
            x1 = min(p[0], x1)
            x2 = max(p[2], x2)

        rects.append([x1, ps[0][1], x2, ps[-1][3]])
    return rects


# draw counter for each cluster
def drawOutline(rects):
    for i in range(len(rects)):
        (x, y, w, h) = rects[i]
        cv2.rectangle(img, (x, y), (w, h), (0, 255, 0), 2)


# get position for each cluster
def getPosition(rects):
    for i in range(len(rects) - 1):
        print('The position between block %s and block %s is: %s' % (
            i + 1, i + 2, relation(rects[i][0], rects[i][2], rects[i + 1][0], rects[i + 1][2])))


if __name__ == "__main__":
    filename = 'image1.PNG'
    img = cv2.imread(filename)
    shape = img.shape
    # get blocks
    data = getBlocks(img)
    type = typeOfImage(data)
    print(type)
    # get outline for cluster of blocks
    rectsGroup = clustering(type)
    # draw outline and show position
    drawOutline(rectsGroup)
    getPosition(rectsGroup)
    # show image
    # cv2.imwrite('Segment_Test/result7.png', img)
    cv2.imshow('image', img)
    cv2.waitKey(0)
