# -*- coding: utf8 -*-

'''
    0 for left
    1 for right
    2 for more than 3 in gradient descent
    3 for no order
'''

import os
import sys
import cv2
from findCluster import getBlocks
from splitComplex import splitCpx
from PIL import Image


# get blocks
def getimginfo(filename):
    img = cv2.imread(filename)
    shape = img.shape
    rects = getBlocks(img)
    tmp, tmpindex = splitCpx().mysplit_ex(rects, shape[1])

    print(filename, tmpindex)
    return tmp, tmpindex


def cmpfun(indexs1, indexs2):
    indexs1set = set(indexs1)
    indexs2set = set(indexs2)
    return indexs1set.issubset(indexs2set)


if __name__ == '__main__':

    if len(sys.argv) == 3:
        imgfile = sys.argv[1]
        cmppath = sys.argv[2]
    else:
        exit()


    tmp, cmpindex = getimginfo(imgfile)
    root = cmppath

    for dirpath, dirnames, filenames in os.walk(root):
        if '.DS_Store' in filenames:
            filenames.remove('.DS_Store')
        else:
            for filepath in filenames:
                fullpath = os.path.join(dirpath, filepath)
                tmp, imgindex = getimginfo(fullpath)
                if cmpfun(cmpindex, imgindex,):
                    tmpim = Image.open(fullpath)
                    tmpim.show()
