# -*- coding: utf8 -*-

import sys
from PIL import Image
from PIL import ImageDraw, ImageFont


def mysplit(d, fun):
    rst = []
    tmp = [d[0], -1]
    for i in range(1, len(d)):
        if fun(d[i], d[i - 1]):
            tmp.append(d[i])
        else:
            rst.append(tmp)
            tmp = [d[i]]
    rst.append(tmp)
    return rst


def fun1(l1, l2):
    if abs(l1[0] - l2[0]) <= 20:
        return True
    return False


def fun2(l1, l2):
    if abs(l1[2] - l2[2]) <= 20:
        return True
    return False


def fun3(l1, l2):
    if l1[0] < l2[0] and l2[0] - l1[0] >= 10:
        return True
    return False


def fun4(l1, l2):
    if l1[2] >= l2[2] and l2[0] - l1[0] >= 10:
        return True
    return False


def iscross(l1, l2):
    if l1[2] < l2[0]:
        return False
    if l1[0] > l2[2]:
        return False
    return True


def gettype(l1, l2):
    funtmp = [fun1, fun2, fun3, fun4]
    rst = set()
    for i, f in enumerate(funtmp):
        if f(l1, l2):
            rst.add(i)
    return rst


def mysplit_ex(data):
    rst = []

    funtmp = [fun1, fun2, fun3, fun4]

    d = data[:]

    while len(d) > 0:

        tmp = [d[0]]
        itype = {0, 1, 2, 3}
        lastp = d[0]
        dremove = [d[0]]

        dsum = 0

        for i in range(1, len(d)):
            if d[i][1] - lastp[3] > 30 or (len(tmp) > 1 and (dsum * 1.0 / len(tmp) * 5 < d[i][1] - lastp[3])):
                break

            if iscross(lastp, d[i]):
                if len(itype) > 0:

                    itype = itype & gettype(lastp, d[i])
                    if len(itype) == 0:
                        break
                    else:
                        dsum += d[i][1] - lastp[3]

                        tmp.append(d[i])
                        lastp = d[i]
                        dremove.append(d[i])

        rst.append(tmp)

        for p in dremove:
            d.remove(p)

    return rst


# get right point
def getrpoint(pix, w, p):
    rst = w - 1
    for x in range(p[0], w):
        if pix[x, p[1]] != (255, 255, 255):
            rst = x
    return rst


# draw the rectangle
def drawrect(imdraw, rect, i=0):
    imdraw.line(((rect[0], rect[1]), (rect[2], rect[1])), fill=(0, 255, 0))
    imdraw.line(((rect[0], rect[1]), (rect[0], rect[3])), fill=(0, 255, 0))

    imdraw.line(((rect[0], rect[3]), (rect[2], rect[3])), fill=(0, 255, 0))
    imdraw.line(((rect[2], rect[1]), (rect[2], rect[3])), fill=(0, 255, 0))


# 水平线段(x1,x2)与(x3,x4)的相对关系
def cmpline(x1, x2, x3, x4):
    if x1 == x2 and x3 == x4:
        return u'Equal'

    elif x1 < x3 and x2 < x3:
        return u'Before'
    elif x4 < x1 and x4 < x2:
        return u'After'

    elif x3 < x1 and x2 < x4:
        return u'During'
    elif x1 < x3 and x4 < x2:
        return u'Contain'

    elif x1 < x3 < x2:
        return u'Overlaps'
    elif x3 < x1 < x4:
        return u'Overlapped-by'

    elif x1 < x3 == x2:
        return u'Meets'
    elif x3 < x1 == x4:
        return u'Met-by'

    elif x1 == x3 and x2 < x4:
        return u'Starts'
    elif x1 == x3 and x4 < x2:
        return u'Started-by'

    elif x3 < x1 and x2 == x4:
        return u'Finishes'
    elif x1 < x3 and x2 == x4:
        return u'Finished-by'


if __name__ == '__main__':

    # 从参数中读取文件名
    if len(sys.argv) >= 2:
        imgfile = sys.argv[1]
    else:
        exit()

    im = Image.open(imgfile)
    frt, size, mode = im.format, im.size, im.mode
    # 图片宽高
    w, h = size
    # 统一转化为rgb模式
    im = im.convert("RGB")

    # 获取所有非背景色最左的点。
    hashpoint = set()
    ltpoints = []
    pix = im.load()
    for y in range(0, h):
        for x in range(0, w):

            if (x * 100000 + y not in hashpoint) and pix[x, y] != (255, 255, 255):

                x2 = w
                y2 = h
                for i in range(x + 1, w):
                    if pix[i, y] == (255, 255, 255):
                        x2 = i - 1
                        break
                for j in range(y + 1, h):
                    if pix[x, j] == (255, 255, 255):
                        y2 = j - 1
                        break

                ltpoints.append([x - 1, y, x2 + 1, y2])

                for i in range(x - 1, x2 + 2):
                    for j in range(y, y2 + 1):
                        hashpoint.add(i * 100000 + j)

                    # 对所有最左边的点，按x坐标连续相等分类
    tmp = mysplit_ex(ltpoints)

    # 分类好后，只需要取最上和最下两个点，也就是一个矩形的左上角，左下角的点
    rects = []
    for ps in tmp:
        x1 = w
        x2 = 0
        for p in ps:
            x1 = min(p[0], x1)
            x2 = max(p[2], x2)

        rects.append([x1, ps[0][1], x2, ps[-1][3]])

    imdraw = ImageDraw.Draw(im)
    for i, x in enumerate(rects):
        drawrect(imdraw, x, i)

    im.show()

    # print the relationship between blocks
    for i in range(len(rects) - 1):
        print('The position between block %s and block %s is: %s' % (
            i + 1, i + 2, cmpline(rects[i][0], rects[i][2], rects[i + 1][0], rects[i + 1][2])))
