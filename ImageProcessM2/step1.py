import cv2


img = cv2.imread("images/mask6.png")
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
# 1. Sobel算子，x方向求梯度
sobel = cv2.Sobel(gray, cv2.CV_8U, 1, 0, ksize=3)
# 2. 二值化
ret, binary = cv2.threshold(sobel, 0, 255, cv2.THRESH_OTSU + cv2.THRESH_BINARY)

# 3. 膨胀和腐蚀操作的核函数
element1 = cv2.getStructuringElement(cv2.MORPH_RECT, (30, 9))
element2 = cv2.getStructuringElement(cv2.MORPH_RECT, (24, 6))

# 4. 膨胀一次，让轮廓突出
dilation = cv2.dilate(binary, element2, iterations=1)

# 5. 腐蚀一次，去掉细节，如表格线等。注意这里去掉的是竖直的线
erosion = cv2.erode(dilation, element1, iterations=1)

# 6. 再次膨胀，让轮廓明显一些
dilation2 = cv2.dilate(erosion, element2, iterations=2)

# erosion1 = cv2.erode(dilation2, element1, iterations=1)

img = dilation2
ret, thresh = cv2.threshold(img, 0, 255, cv2.THRESH_BINARY_INV)
cv2.imwrite('160.png', thresh)

origin_pic = cv2.imread('160.png')
# 文档路径，用于记录轮廓框坐标
# 要先转换成单通道灰度图像才能进行后续的图像处理
pic = cv2.cvtColor(origin_pic, cv2.COLOR_BGR2GRAY)
# 阈值处理，将前景全填充为白色，背景全填充为黑色
_, pic = cv2.threshold(src=pic, thresh=200, maxval=255, type=1)
# 中值滤波，去除椒盐噪声
pic = cv2.medianBlur(pic, 5)
# 边缘检测，得到的轮廓列表
contours, _2 = cv2.findContours(pic, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
# 根据轮廓列表，循环在原始图像上绘制矩形边界
for i in range(len(contours)):
    cnt = contours[i]
    x, y, w, h = cv2.boundingRect(cnt)
    origin_pic = cv2.rectangle(origin_pic, (x, y), (x+w, y+h), (0, 0, 0), 2)

# cv2.imwrite('img4.png', origin_pic)

cv2.imshow('', origin_pic)
cv2.waitKey(0)
cv2.destroyAllWindows()


