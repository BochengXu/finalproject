# -*- coding: utf8 -*-
from PIL import Image, ImageDraw


def fun1(l1, l2):
    if abs(l1[0] - l2[0]) <= 20:
        return True
    return False


def fun2(l1, l2):
    if abs(l1[2] - l2[2]) <= 20:
        return True
    return False


def fun3(l1, l2):
    if l1[0] < l2[0] and l2[0] - l1[0] >= 10:
        return True
    return False


def fun4(l1, l2):
    if l1[2] >= l2[2] and l2[0] - l1[0] >= 10:
        return True
    return False


def iscross(l1, l2):
    print(l1, l2)
    if l1[2] < l2[0]:
        return False
    if l1[0] > l2[2]:
        return False
    return True


def gettype(l1, l2):
    funtmp = [fun1, fun2, fun3, fun4]
    rst = set()
    for i, f in enumerate(funtmp):
        if f(l1, l2):
            rst.add(i)
    return rst


def mysplit_ex(data):
    rst = []
    d = data[:]
    while len(d) > 0:

        tmp = [d[0]]
        itype = {0, 1, 2, 3}
        lastp = d[0]
        dremove = [d[0]]

        dsum = 0

        for i in range(1, len(d)):
            if d[i][1] - lastp[3] > 30 or (len(tmp) > 1 and (dsum * 1.0 / len(tmp) * 5 < d[i][1] - lastp[3])):
                break

            if iscross(lastp, d[i]):
                if len(itype) > 0:
                    itype = itype & gettype(lastp, d[i])
                    print(gettype(lastp, d[i]))
                    if len(itype) == 0:
                        break
                    else:
                        dsum += d[i][1] - lastp[3]

                        tmp.append(d[i])
                        lastp = d[i]
                        dremove.append(d[i])

        rst.append(tmp)

        for p in dremove:
            d.remove(p)

    return rst


# 水平线段(x1,x2)与(x3,x4)的相对关系
def cmpline(x1, x2, x3, x4):
    if x1 == x2 and x3 == x4:
        return u'Equal'

    elif x1 < x3 and x2 < x3:
        return u'Before'
    elif x4 < x1 and x4 < x2:
        return u'After'

    elif x3 < x1 and x2 < x4:
        return u'During'
    elif x1 < x3 and x4 < x2:
        return u'Contain'

    elif x1 < x3 < x2:
        return u'Overlaps'
    elif x3 < x1 < x4:
        return u'Overlapped-by'

    elif x1 < x3 == x2:
        return u'Meets'
    elif x3 < x1 == x4:
        return u'Met-by'

    elif x1 == x3 and x2 < x4:
        return u'Starts'
    elif x1 == x3 and x4 < x2:
        return u'Started-by'

    elif x3 < x1 and x2 == x4:
        return u'Finishes'
    elif x1 < x3 and x2 == x4:
        return u'Finished-by'


if __name__ == '__main__':

    im = Image.open('images/mask5.png')
    w, h = im.size
    # convert image to RGB
    im = im.convert("RGB")

    # get top-left and bottom-right point for each block
    hashpoint = set()
    ltpoints = []
    pix = im.load()
    for y in range(0, h):
        for x in range(0, w):

            if (x * 100000 + y not in hashpoint) and pix[x, y] != (255, 255, 255):

                x2 = w
                y2 = h
                for i in range(x + 1, w):
                    if pix[i, y] == (255, 255, 255):
                        x2 = i - 1
                        break

                for j in range(y + 1, h):
                    if pix[x, j] == (255, 255, 255):
                        y2 = j - 1
                        break

                if (x2 + 1 - x - 1) * (y2 - y) > 230:
                    ltpoints.append([x - 1, y, x2 + 1, y2])

                for i in range(x - 1, x2 + 2):
                    for j in range(y, y2 + 1):
                        hashpoint.add(i * 100000 + j)

    # classify the blocks
    tmp = mysplit_ex(ltpoints)

    # 分类好后，只需要取最上和最下两个点，也就是一个矩形的左上角，左下角的点
    rects = []
    for ps in tmp:
        x1 = w
        x2 = 0
        for p in ps:
            x1 = min(p[0], x1)
            x2 = max(p[2], x2)

        rects.append([x1, ps[0][1], x2, ps[-1][3]])

    # Draw rectangle on original iamge
    imdraw = ImageDraw.Draw(im)
    for i, x in enumerate(rects):
        imdraw.rectangle(((x[0], x[1]), (x[2], x[3])), outline='red', width=2)

    # print the relationship between blocks
    for i in range(len(rects) - 1):
        print('The position between block %s and block %s is: %s' % (
            i + 1, i + 2, cmpline(rects[i][0], rects[i][2], rects[i + 1][0], rects[i + 1][2])))

    im.show()
