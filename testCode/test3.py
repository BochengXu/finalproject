# -*- coding: utf8 -*-

import sys
from PIL import Image
from PIL import ImageDraw, ImageFont


# 自己的分类函数
# [1,2,3,3,4,4]=>[[1], [2], [3,3], [4,4]]这样
def mysplit(d, fun):
    rst = []
    tmp = [d[0], -1]
    for i in range(1, len(d)):
        if fun(d[i], d[i - 1]):
            tmp.append(d[i])
        else:
            rst.append(tmp)
            tmp = [d[i]]
    rst.append(tmp)
    return rst


# 是否在图片左边
def isleft(l, width):
    if (l[0] + l[2]) / 2 <= width / 2:
        return 0
    else:
        return 1


# 是否同为类型1或同为类型2
def fun1(l1, l2, width):
    if isleft(l1, width) == isleft(l2, width) and (abs(l1[0] - l2[0]) <= 50 or abs(l1[2] - l2[2]) <= 50):
        return True
    return False


# 类型3
def fun3(l1, l2):
    if l2[0] > l1[0]:
        return True
    return False


def mysplit_ex(data, width):
    rst = []

    dlen = len(data)
    flag = [-1 for _ in range(dlen)]

    # 最先找出类型3（梯度下降的类型），对于是梯度为3的标为3,8888,8888,8888
    i = 0
    while i < dlen - 2:
        k = 0
        while i + k < dlen - 1:
            if fun3(data[i + k], data[i + k + 1]):
                k += 1
            else:
                break
        if k >= 2:
            flag[i] = 3
            for x in range(1, k + 1):
                flag[i + x] = 8888

        i = i + k + 1

    # 分割
    for i in range(dlen):
        # 为4表示已经分割了
        if flag[i] == 4:
            continue
        # 为3表示类型3梯度下降
        if flag[i] == 3:
            k = 1
            tmp = [data[i]]
            while i + k < dlen:
                if flag[i + k] == 8888:
                    tmp.append(data[i + k])
                    flag[i + k] = 4
                else:
                    break
                k += 1

            rst.append([3, tmp])

        # 末分割
        if flag[i] == -1:
            k = 1
            lastindex = i
            tmp = [i]
            # 标示为已分割
            flag[lastindex] = 4
            while i + k < dlen:
                # 类型1与类型2相互交错不可大于2，或者不能被类型3断开
                if i + k - lastindex > 2 or flag[i + k] == 3:
                    break

                # 本块左边与上一块右边不超过15
                if data[i + k][1] - data[i + k - 1][3] >= 15:
                    break

                # 本块与上一块属于类型1或类型2，划为一大块
                if flag[i + k] == -1 and fun1(data[i + k], data[lastindex], width):
                    tmp.append(i + k)
                    lastindex = i + k
                k = k + 1

            # 大于2为类型1，2，3否则为类型4无规则
            if len(tmp) > 2:

                ttmp = []
                for x in tmp:
                    ttmp.append(data[x])
                    flag[x] = 4
                rst.append([isleft(data[i], width), ttmp])
            else:
                rst.append([2, [data[i]]])

    # for p in rst:
    #	print 'rst', p

    rstex = []
    rstindex = []
    i = 0
    while i < len(rst):
        if rst[i][0] != 2:
            rstex.append(rst[i][1])
            rstindex.append(rst[i][0])
            i += 1
        # 如果连续无规划的划为一类
        else:
            tmp = rst[i][1]
            k = 1
            while i + k < len(rst):
                if rst[i + k][0] == 2:
                    tmp = tmp + rst[i + k][1]
                else:
                    break
                k += 1
            rstex.append(tmp)
            rstindex.append(2)
            i += k

    # for p in rstex:
    #	print 'rstex', p

    return rstex, rstindex


def getimginfo(filename):
    im = Image.open(filename)  ##文件存在的路径
    frt, size, mode = im.format, im.size, im.mode
    # 图片宽高
    w, h = size
    width = w
    # 统一转化为rgb模式
    im = im.convert("RGB")

    # im.show()

    # 获取所有非背景色最左的点。
    hashpoint = set()
    ltpoints = []
    pix = im.load()
    for y in range(0, h):
        for x in range(0, w):

            if (x * 100000 + y not in hashpoint) and pix[x, y] != (255, 255, 255):

                x2 = w
                y2 = h
                for i in range(x + 1, w):
                    if pix[i, y] == (255, 255, 255):
                        x2 = i - 1
                        break
                for j in range(y + 1, h):
                    if pix[x, j] == (255, 255, 255):
                        y2 = j - 1
                        break

                if x2 - x > 5 and y2 - y > 5:
                    ltpoints.append([x - 1, y, x2 + 1, y2])

                    for i in range(x - 1, x2 + 2):
                        for j in range(y, y2 + 1):
                            hashpoint.add(i * 100000 + j)

                        # print filename, ltpoints, width
    # 对所有最左边的点，按x坐标连续相等分类
    tmp, tmpindex = mysplit_ex(ltpoints, width)

    # print tmp, tmpindex

    return tmp, tmpindex


def cmpfun(indexs1, indexs2, cmptype):
    if cmptype == 1:

        indexs1set = set(indexs1)
        indexs2set = set(indexs2)
        return indexs1set.issubset(indexs2set)

    elif cmptype == 2:
        indexs1str = ''.join(map(str, indexs1))
        indexs2str = ''.join(map(str, indexs2))
        return indexs1str in indexs2str

    elif cmptype == 3:

        tmpi = 0
        for p1 in indexs1:
            if p1 in indexs2[tmpi:]:
                tmpi = indexs2.index(p1) + 1
            else:
                return False
        return True


if __name__ == '__main__':

    # 从参数中读取文件名
    if len(sys.argv) >= 3:
        imgfile = sys.argv[1]
        cmppath = sys.argv[2]
        cmptype = 1  # int(sys.argv[3])
    elif len(sys.argv) >= 4:
        imgfile = sys.argv[1]
        cmppath = sys.argv[2]
        cmptype = int(sys.argv[3])
    else:
        exit()

    tmp, cmpindex = getimginfo(imgfile)

    import os

    root = cmppath

    for dirpath, dirnames, filenames in os.walk(root):
        filenames.remove('.DS_Store')
        for filepath in filenames:
            fullpath = os.path.join(dirpath, filepath)

            tmp, imgindex = getimginfo(fullpath)
            if cmpfun(cmpindex, imgindex, cmptype):
                tmpim = Image.open(fullpath)
                tmpim.show()
