import cv2
import numpy as np

img = cv2.imread('image.PNG')
shape = img.shape
# Gray image
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
# Threshold
ret, thres_img = cv2.threshold(gray, 200, 255, cv2.THRESH_BINARY_INV)

# Morphology Operation
kernel = np.ones((1, 10), np.uint8)
maskClose = cv2.morphologyEx(thres_img, cv2.MORPH_CLOSE, kernel)

kernel = np.ones((1, 1), np.uint8)
maskOpen = cv2.morphologyEx(maskClose, cv2.MORPH_OPEN, kernel)

cv2.imshow('open', maskClose)
# find counter
(cnts, _) = cv2.findContours(maskOpen.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

Rectlist = []

# judge the area and the rate of width and height for the outline
for i in range(len(cnts)):
    counter = cnts[i]
    data = counter
    (x, y, w, h) = cv2.boundingRect(data)
    # if w > int(shape[1] / 50) and w > h and w * h > 100:
    Rectlist.append((x, y, w, h))
        # cv2.rectangle(img, (x - 1, y - 1), (x + w + 2, y + h + 2), (0, 0, 0), -1)
        # cv2.drawContours(img, cnts, i, (255, 0, 0), 1)

# convert order (4, 3, 2, 1) -> (1, 2, 3, 4)
# Rectlist = sorted(Rectlist, key=lambda block: block[1])
# check if blocks in one line
# for i in range(len(Rectlist)):
#     if i > 0 and Rectlist[i][1] - Rectlist[i - 1][1] < Rectlist[i][3] * 0.7:
#         x = min(Rectlist[i - 1][0], Rectlist[i][0])
#         y = min(Rectlist[i - 1][1], Rectlist[i][1])
#         w = max(Rectlist[i - 1][0] + Rectlist[i - 1][2], Rectlist[i][0] + Rectlist[i][2]) - x
#         h = max(Rectlist[i - 1][1] + Rectlist[i - 1][3], Rectlist[i][1] + Rectlist[i][3]) - y
#         Rectlist[i] = (x, y, w, h)
#         Rectlist[i - 1] = (0, 0, 0, 0)

# image = np.ones((shape[0], shape[1]), dtype=np.uint8)
# for i in range(shape[0]):
#     for j in range(shape[1]):
#         image[i, j] = 255

# draw rectangle on original image
for i in range(len(Rectlist)):
    (x, y, w, h) = Rectlist[i]
    cv2.rectangle(img, (x, y - 1), (x + w, int(y + h * 0.9)), (0, 0, 255), 1)


cv2.imshow("result1", img)
cv2.waitKey(0)
